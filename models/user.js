const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/yelp_camp', { useNewUrlParser: true, useUnifiedTopology: true }); 
const passportLocalMongoose =require("passport-local-mongoose");

const UserSchema = new mongoose.Schema({
    username:String,
    password:String
});

UserSchema.plugin(passportLocalMongoose);
module.exports =mongoose.model("User", UserSchema);