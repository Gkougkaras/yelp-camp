const express = require("express");
const router =express.Router();
const Campground = require("../models/campground");
const middleware = require("../middleware");

//INDEX ROUTE
router.get("/", function(req, res){
    Campground.find({},function(err,allCampgrounds){
        if (err){
           console.log("ERROR");
        }
        else{
         res.render("campgrounds/index", {campgrounds: allCampgrounds});
        }
    });
 });
 
 //POST ROUTE
 router.post("/", middleware.isLoggedIn,function(req, res){
     const name = req.body.name;
     const image = req.body.image;
     const description = req.body.description;
     const price =req.body.price;
     const author = {
         id:req.user._id,
         username:req.user.username
     }
     const newCampground = {name: name, image: image, description: description ,author:author, price: price}
     Campground.create(newCampground,function(err,newlyCreated){
         if (err){
             console.log(err);
         }
         else{
             res.redirect("/campgrounds");
         }
 });
 
 });
 
 //NEW ROUTE
 router.get("/new",middleware.isLoggedIn, function(req, res){
 res.render("campgrounds/new");
 })
 
 //SHOW ROUTE
 router.get("/:id",function(req, res){
     Campground.findById(req.params.id).populate("comments").exec(function(err,foundCampground){
         if (err || !foundCampground){
             req.flash("error", "Campground not found");
             res.redirect("back");
         }
         else{
             res.render("campgrounds/show", {campground: foundCampground});
         }
     });
 });

 //EDIT CAMPGROUND ROUTE
    router.get("/:id/edit",middleware.checkCampgroundOwnership,function(req, res){
            Campground.findById(req.params.id, function(err,foundCampground){
                 res.render("campgrounds/edit", {campground: foundCampground});
            });
        });

 //UPDATE CAMPGROUND ROUTE
    router.put("/:id",middleware.checkCampgroundOwnership,function(req,res){
        Campground.findByIdAndUpdate(req.params.id,req.body.campground,function(err,updatedCampground){
            if (err){
                res.redirect("/campgrounds");
            }else{
                res.redirect("/campgrounds/"+ req.params.id);
            }
        });
    });

//DESTROY CAMPGROUND ROUTE

    router.delete("/:id",middleware.checkCampgroundOwnership,function(req, res){
        Campground.findByIdAndRemove(req.params.id, function(err){
            if (err){
                res.redirect("/campgrounds");
            }else{
                res.redirect("/campgrounds");
            }
        });
    });


 module.exports = router;