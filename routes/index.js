const express = require("express");
const router =express.Router();
const passport=require("passport");
const User = require("../models/user");

//ROOT ROUTE
router.get("/", function(req, res){
    res.render("landing");
})

//SHOW REGISTER FORM
router.get("/register",function(req,res){
    res.render("register");
});

//SIGN UP LOGIC ROUTE
router.post("/register",function(req,res){
    const newUser = new User({username: req.body.username});
    User.register(newUser,req.body.password,function(err,user){
        if(err){
            req.flash("error", err.message);
            res.redirect("back");
        }
        passport.authenticate("local")(req,res , function(){
            req.flash("success", "Welcome to YelpCamp "+ user.username);
            res.redirect("/campgrounds");
        });
    });
});

//SHOW LOGIN FORM

router.get("/login",function(req,res){
    res.render("login");
});

//LOGIN LOGIC ROUTE
router.post("/login", passport.authenticate("local",{
    successRedirect:"/campgrounds",
    failureRedirect:"/login"}),
    function(req,res){
    });

//LOG OUT LOGIC ROUTE

router.get("/logout",function(req,res){
    req.logout();
    req.flash("success", "Logged you out!!");
    res.redirect("/campgrounds");
});


module.exports = router;