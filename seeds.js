const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/yelp_camp', { useNewUrlParser: true, useUnifiedTopology: true }); 

const Campground = require("./models/campground");
const Comment =require("./models/comment");

const data =[
    { name:"Clouds Rest",
      image:"https://pixabay.com/get/50e9d4474856b10ff3d8992ccf2934771438dbf85254794f7c2d7ad19e4e_340.jpg",
      description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."},
    {name:"Mt Olympus",
    image:"https://pixabay.com/get/52e8d4444255ae14f1dc84609620367d1c3ed9e04e507441732679d59244c6_340.jpg",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."},
    {name:"Night Fire",
    image:"https://pixabay.com/get/54e5dc474355a914f1dc84609620367d1c3ed9e04e507441732679d59244c6_340.jpg",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}
]
//REMOVE ALL CAMPGROUNDS
function seedDB(){
    Campground.remove({},function(err){
        if (err){
            console.log(err);
        }
        console.log("Database Cleared");
        //CREATE A FEW CAMPGROUNDS
    data.forEach(function(seed){
        Campground.create(seed,function(err,campground){
            if(err){
                console.log(err);
            }
            else{
                console.log("Added a campground");
                //CREATE A COMMENT
                Comment.create(
                    {
                    text:"This place is great",
                    author:"Bill"
                    },
                    function(err,comment){
                    if(err){
                        console.log(err);
                    }
                    else{
                        campground.comments.push(comment);
                        campground.save();
                        console.log("Created a comment");
                    }
                })
            }
        });
    });
    });
}

    

module.exports = seedDB;